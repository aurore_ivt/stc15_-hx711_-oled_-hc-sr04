#include "reg52.h"
#include "intrins.h"

void InitIRQ(void);
void Conut(void);
void delayms(unsigned int ms);
void StartModule();	

#define RX  P11
#define TX  P12
  
unsigned int  time=0;
float         Distance=0;//距离
bit           flag =0;   //中断溢出标志

/*********** 中断寄存器设置初始化 ***********/
void InitIRQ(void)
{
	TMOD=0x21;  //T/C工作方式寄存器 0010 0001
	            //T0 ：GATE=0; 定时模式； 工作方式1，16位T/C； 计数器溢出中断，用于判定超出测距范围
	            //T1 ：GATE=0; 定时模式； 工作方式2，8位可自动重载T/C； 用于串口通信 波特率发生器
	SCON=0x50;  //串行口控制寄存器  0101 0000 
	            //SM0 SM1：工作方式1；10位异步收发；波特率由定时器T1控制
	            //SM2: 多机通信控制位，方式0和方式1为非多机通信，设置0
	            //REN: 串行口接收允许位，允许串行口接收数据
	            //TB8：方式0和方式1中该位不用
	            //RB8: 方式0和方式1中该位不用
	            //TI: 发送中断标志位。串行发送停止位时，由内部硬件置1，向CPU发中断申请，必须由软件清0
             	//RI: 接收中断标志位。串行接收停止位时，由内部硬件置1，向CPU发中断申请，必须由软件清0
	TH0=0;      //T0初始化，，用于判断测距溢出，最大65.536 ms
	TL0=0; 	
	TH1=0xFD;   //T1初始化  设定波特率9600 （波特率计算参考文档）
	TL1=0xFD;   
	
	ET0=1;      //T0中断允许
	TR0=1;      //开启定时器0
	TR1=1;      //开启定时器1
	//ES=1;     //串口允许中断
	            /*
							  可删除，我的理解是：在系统正常运行情况下，串口发送完成则置位TI=1,
							  向CPU请求中断，在我们软件人为的直接设置TI=1的情况下可以直接向CPU
							  请求中断了，已经跳过了“允许”那一步
								并且最好删除！
								因为TI为中断标志位如果程序使用了串口中断，那么每次调用printf都
								会进入中断，因此在使用printf前要禁用中断
							*/
	TI=1;       /*
	             发送中断标志 ☆直接使用printf必须加此语句才能发送
               在KEILC中，printf在传输数据前需要确保前一个数据传输
							 结束，也就是TI=1，否则将处于等待状态
							 因为printf函数会调用putchar函数，而putchar函数会判断TI，
							 不为1则等待(相当于死机)，为1则清0，发送完成后又自动置1
							 因此第一次运行printf时检查TI=1则进行发送，发送完成后
							 发送中断标志位TI又自动置1
							 */
  EA=1;       //开启总中断
}

/*********** 触发超声波模块 ***********/
void  StartModule() //超声波模块Trig控制端给大于10us的高电平触发模块测距	
{
	  TX=1;     
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  //_nop_(); 
	  //_nop_();
	  //_nop_(); 
	  //_nop_(); 
	  //_nop_(); 
	  TX=0;
}

/*********** 定时器T0计数 ***********/
void Conut(void)
{
	 time=TH0*256+TL0;
	 TH0=0;
	 TL0=0;
	 Distance = (time*1.87)/100;  //CM  (见代码最后注释)
	 if(flag==1)		    //超出测量
	 {
	  flag=0;
	 }
}
/********************************************************/ 
void delayms(unsigned int ms)
{
	unsigned char i=100,j;
	for(;ms;ms--) 
	{
		while(--i)
		{
			j=10;
			while(--j);
		}
	}
}
/*********** 定时器T0中断服务函数 ***********/
void Timer0IRQ() interrupt 1 //T0中断用来计数器溢出,超过测距范围
{
    flag=1;	
}

/*12分频:就是f/12，假设（接晶振12MHz）输入信号频率12MHz，12分频后，则输出1MHz
	*时钟周期周期变为原来的12倍， T=1/1MHz=1us
	*即单片机内部的加1计数器在加1这个过程中 ，寄存器要完成这个动作，是一个机器周期
	*时钟周期为1/12MHz=1/12us
	*机器周期=12个时钟周期=1us，也就是计数器每加1需要的时间问1us
	*单片机中的部件都是在晶振12分频后的一个机器周期在跑
	*时钟周期 = 晶振频率的倒数，即1/fosc
	*机器周期 = 12 * 时钟周期 = 12/fosc
	------------------------------------------------------
	|距离计算公式
	|2S(m) = t(s) * 344(m/s)
	|S(m)  = t(s) * 172(m/s)
	|S(cm) = t(us) * 0.0172(cm/us)
	|      = t(us) * 1/58
	|t在晶振位12MHz时等于计数值（机器周期1us），但晶振为11.0592MHz时
	|t(us) = 计数 * （12/11.0592) * (1/58)
	|      = 计数 * 0.0187
	|      = (计数 * 1.87)/100
	------------------------------------------------------
	*/              