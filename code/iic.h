#ifndef _IIC_H
#define _IIC_H
 
#include "main.h"
 
void IIC_Start(void); 
bit IIC_WaitAck(void);  
void IIC_SendAck(bit ackbit); 
void IIC_SendByte(unsigned char byt); 
unsigned char IIC_RecByte(void); 
 
#endif