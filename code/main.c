#include "reg52.h"
#include "oled.h"
#include "HX711.h"
#include "intrins.h"


//sfr T2CON = 0xc8;
//sfr T2MOD = 0xc9;
//sfr RCAP2H = 0xca;
//sfr RCAP2L = 0xcb;
//sfr TL2 = 0xcC;
//sfr TH2 = 0xcd;
//sbit ET2   = IE^5; 

//sbit TF2 = T2CON^7;
//sbit TR2 = T2CON^2;
//******************定时器1
void Timer1Init(void);
void key_s(void);
//******************称重传感器
unsigned long HX711_Buffer = 0;
unsigned int Weight_Maopi = 0,Weight_Shiwu = 0;
void Get_Weight();
void Get_Maopi();
//float m__1=16.752;
float m__1=2;
int key=0;
void Delay500ms();
void Delay_1ms(unsigned int Del_1ms);
int BMI=0;
//******************测距传感器

void InitIRQ(void);
void Conut(void);
void delayms(unsigned int ms);
void StartModule();	
#define RX  P11
#define TX  P12
extern float  Distance;//距离

void main(void)
{	
//		unsigned int t=1;

		OLED_Init();			//初始化OLED  
		OLED_Clear(); 		//清屏函数
		Get_Maopi();				//称毛皮重量
		Delay500ms();
		InitIRQ();
		Timer1Init();
		P00=1;P01=1;P10=1;
		while(1) 
		{			
			 StartModule();
			 while(!RX);		//当超声波模块接收口输出低电平则等待
			 TR0=1;	        //开启计数
			 while(RX);			//当RX为1计数并等待
			 TR0=0;				  //关闭计数
			 Conut();			  //读取定时器的值，计算
			
			Delay_1ms(5);
			if(P13==0)key=1;
			if(P14==0)key=2;
			
			if(key==1){P10=0;ET1 = 1;}
			if(key==2){P10=1;ET1 = 0;}
			Get_Weight();
			
			BMI=Weight_Shiwu*2/(200-Distance);
			
		}	  
}

//****************************************************
//称重
//****************************************************
void Get_Weight()
{	
	HX711_Buffer = HX711_Read();
	HX711_Buffer = HX711_Buffer/100;
	if(HX711_Buffer > Weight_Maopi)			
	{
		Weight_Shiwu = HX711_Buffer;
		Weight_Shiwu = Weight_Shiwu - Weight_Maopi;				//获取实物的AD采样数值。
	
		Weight_Shiwu = (unsigned int)((float)Weight_Shiwu/m__1+0.05); 	
																		//计算实物的实际重量
																		//因为不同的传感器特性曲线不一样，因此，每一个传感器需要矫正这里的4.30这个除数。
																		//当发现测试出来的重量偏大时，增加该数值。
																		//如果测试出来的重量偏小时，减小改数值。
																		//该数值一般在4.0-5.0之间。因传感器不同而定。
																		//+0.05是为了四舍五入百分位
		//Buzzer = 1;				//关闭警报
	}
	
}
void Get_Maopi()
{
	HX711_Buffer = HX711_Read();
	Weight_Maopi = HX711_Buffer/100;		
} 

void Delay500ms()		//@11.0592MHz
{
	unsigned char i, j, k;

	_nop_();
	i = 4;
	j = 129;
	k = 119;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}

void Timer1Init(void)		//1毫秒@11.0592MHz
{
	AUXR |= 0x40;		//定时器时钟1T模式
	TMOD &= 0x0F;		//设置定时器模式
	TL1 = 0xCD;		//设置定时初值
	TH1 = 0xD4;		//设置定时初值
	TF1 = 0;		//清除TF1标志
	TR1 = 1;		//定时器1开始计时
	ET1 = 1;
}
void Timer2Init(void)		//1毫秒@11.0592MHz
{
TCLK=0;     //可令T2CON=0；或TCLK=0,RCLK=0;
RCLK=0;    //【T2CON中其他位可默认为0，而TCLK和RCLK必须手动置0】
      //因RCAP2L和RCAP2H是由软件预设的
    T2MOD=0x00;
    RCAP2H=0x3C;
    RCAP2L=0xB0;

TL2 = (65536-50000) % 256;  //或TL2=0xb0
TH2 = (65536-50000) / 256;  //或TH2=0x3c
EA = 1;
ET2 = 1;
TR2 = 1;
	
}
/*********** 定时器T1中断服务函数 ***********/
void Timer1IRQ() interrupt 3 //T0中断用来计数器溢出,超过测距范围
{
			char s;
			s++;
			
			if(s==100)
			{
   			OLED_ShowCHinese(0,0,0);//	//第一个形参是列，第二个是行，第三个是显示哪个中文，这里一个字占16宽
				OLED_ShowCHinese(18,0,1);//
				OLED_ShowCHinese(36,0,2);//
				OLED_ShowNum(54,0,key,1,16);
				OLED_ShowCHinese(0,2,3);OLED_ShowCHinese(16,2,4);OLED_ShowNum(32,2,200-Distance,3,16);OLED_ShowString(56,2,"CM",16);
				OLED_ShowCHinese(0,4,5);OLED_ShowCHinese(16,4,6);OLED_ShowNum(32,4,Weight_Shiwu,4,16);OLED_ShowString(72,4,"g",16);
				OLED_ShowString(0,6,"BMI",16);OLED_ShowCHinese(24,6,7);OLED_ShowCHinese(40,6,8);OLED_ShowNum(58,6,BMI,3,16);
				if(BMI<18)
				{
					OLED_ShowCHinese(85,6,9);
					OLED_ShowCHinese(101,6,10);
				}
				else if(BMI>24 && BMI<24)
				{
					OLED_ShowCHinese(85,6,11);
					OLED_ShowCHinese(101,6,12);
				}
				else if(BMI>28)
				{
					OLED_ShowCHinese(85,6,13);
					OLED_ShowCHinese(101,6,14);
				}
			}
}

void Timer2IRQ() interrupt 5 
{
	EA = 0;
	TF2 = 0;          //必须软件置0
	
	key_s();
	P10=0;
	OLED_ShowNum(54,0,key,1,16);
	
	EA = 1;
}
void key_s(void)
{
	P00=1;P01=1;
	if(P00==0)
	{
		if(P00==0)key=1;
	}
	if(P24==0)
	{
		if(P24==0)key=2;
	}
}


